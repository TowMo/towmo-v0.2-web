'use strict';

/**
 * @ngdoc function
 * @name towmoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the towmoApp
 */
angular.module('towmo.controllers', 
  [ 'towmo.services', 
    'famous.angular'  ])
.controller('AppCtrl', 
  function($scope, User, SiteGeo, $location, AppAuth) {
    AppAuth.ensureHasCurrentUser(User);

    $scope.currentUser = AppAuth.currentUser;
    $scope.getSiteGeo = function() {
      console.log(SiteGeo.find(""));

    }

    $scope.logout = function() {
      User.logout(
        function() {
        console.log('success logout');
        $scope.currentUser =
        AppAuth.currentUser = null;
        $location.path('/');
      });
    }
  }
)
.controller('LoginCtrl', 
  function($scope, $routeParams, User, $location, AppAuth) {

    // $scope.registration = {};
    $scope.credentials = {
      username: 'ggonzalez',
      password: 'ggonzalez'
    };

    $scope.login = 
      function() {
        $scope.loginResult = 
          User.login( {include: 'User', rememberMe: true}, $scope.credentials,
            function() {
              var next = $location.nextAfterLogin || '/main';
              $location.nextAfterLogin = null;
              AppAuth.currentUser = $scope.loginResult.user;
              $location.path(next);
            },
            function(res) {
              $scope.loginError = res.data.error;
            }
          );
      }
  }
)
.controller('SummaryCtrl', ['$scope', '$famous', function($scope, $famous) {
        
  var EventHandler = $famous['famous/core/EventHandler'];

  $scope.views = [{color: 'red'}, {color: 'blue'}, {color: 'green'}, {color: 'yellow'}, {color: 'orange'}];

  $scope.myEventHandler = new EventHandler();

}]);

