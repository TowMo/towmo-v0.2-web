'use strict';

/**
 * @ngdoc overview
 * @name towmoApp
 * @description
 * # towmoApp
 *
 * Main module of the application.
 */
angular
  .module('towmo', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'lbServices',
    'famous.angular',
    'towmo.services',
    'towmo.controllers'
  ])
  // .config(['$httpProvider', function($httpProvider) {
  //       $httpProvider.defaults.useXDomain = true;
  //       delete $httpProvider.defaults.headers.common['X-Requested-With'];
  //   }
  // ])
  .config(function ($compileProvider){
    // Needed for routing to work
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
  })
  .config(function ($routeProvider, $locationProvider, $httpProvider) {

    $routeProvider
      .when('/main', {
        templateUrl: 'views/app.html',
        controller: 'AppCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .otherwise({
        redirectTo: '/login'
      });

    // Intercept 401 responses and redirect to login screen
    $httpProvider.interceptors.push(function($q, $location, AppAuth) {
      return {
        responseError: function(rejection) {
          console.log(rejection);
          console.log('intercepted rejection of ', rejection.config.url, rejection.status);
          if (rejection.status == 401) {
            AppAuth.currentUser = null;
            // save the current location so that login can redirect back
            $location.nextAfterLogin = $location.path();
            $location.path('/login');
          }
          return $q.reject(rejection);
        }
      };
    });
  })
  .run(function($rootScope, $location, AppAuth) {
  $rootScope.$on("$routeChangeStart", function(event, next, current) {
    console.log('AppAuth.currentUser', AppAuth.currentUser);
    console.log('$location.path()', $location.path());
  });
  });
