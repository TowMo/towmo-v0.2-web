(function(window, angular, undefined) {'use strict';

var urlBase = "https://themis.hyperdeck.net:3000/api";

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.Container
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Container` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Container",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/containers/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Container#getContainers
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getContainers": {
          url: urlBase + "/containers",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#createContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "createContainer": {
          url: urlBase + "/containers",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#destroyContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "destroyContainer": {
          url: urlBase + "/containers/:container",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getContainer": {
          url: urlBase + "/containers/:container",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getFiles
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getFiles": {
          url: urlBase + "/containers/:container/files",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getFile
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getFile": {
          url: urlBase + "/containers/:container/files/:file",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#removeFile
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "removeFile": {
          url: urlBase + "/containers/:container/files/:file",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#upload
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `req` – `{object=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `result` – `{object=}` - 
         */
        "upload": {
          url: urlBase + "/containers/:container/upload",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#download
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "download": {
          url: urlBase + "/containers/:container/download/:file",
          method: "GET",
        },
      }
    );





    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.State
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `State` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "State",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/states/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use State.vehicles.findById() instead.
        "prototype$__findById__vehicles": {
          url: urlBase + "/states/:id/vehicles/:fk",
          method: "GET",
        },

        // INTERNAL. Use State.vehicles.destroyById() instead.
        "prototype$__destroyById__vehicles": {
          url: urlBase + "/states/:id/vehicles/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use State.vehicles.updateById() instead.
        "prototype$__updateById__vehicles": {
          url: urlBase + "/states/:id/vehicles/:fk",
          method: "PUT",
        },

        // INTERNAL. Use State.sites.findById() instead.
        "prototype$__findById__sites": {
          url: urlBase + "/states/:id/sites/:fk",
          method: "GET",
        },

        // INTERNAL. Use State.sites.destroyById() instead.
        "prototype$__destroyById__sites": {
          url: urlBase + "/states/:id/sites/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use State.sites.updateById() instead.
        "prototype$__updateById__sites": {
          url: urlBase + "/states/:id/sites/:fk",
          method: "PUT",
        },

        // INTERNAL. Use State.vehicles() instead.
        "prototype$__get__vehicles": {
          url: urlBase + "/states/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use State.vehicles.create() instead.
        "prototype$__create__vehicles": {
          url: urlBase + "/states/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use State.vehicles.destroyAll() instead.
        "prototype$__delete__vehicles": {
          url: urlBase + "/states/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use State.vehicles.count() instead.
        "prototype$__count__vehicles": {
          url: urlBase + "/states/:id/vehicles/count",
          method: "GET",
        },

        // INTERNAL. Use State.sites() instead.
        "prototype$__get__sites": {
          url: urlBase + "/states/:id/sites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use State.sites.create() instead.
        "prototype$__create__sites": {
          url: urlBase + "/states/:id/sites",
          method: "POST",
        },

        // INTERNAL. Use State.sites.destroyAll() instead.
        "prototype$__delete__sites": {
          url: urlBase + "/states/:id/sites",
          method: "DELETE",
        },

        // INTERNAL. Use State.sites.count() instead.
        "prototype$__count__sites": {
          url: urlBase + "/states/:id/sites/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#create
         * @methodOf lbServices.State
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/states",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#upsert
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/states",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#exists
         * @methodOf lbServices.State
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/states/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#findById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/states/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#find
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/states",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.State#findOne
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/states/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#updateAll
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/states/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#deleteById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/states/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#count
         * @methodOf lbServices.State
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/states/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$updateAttributes
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/states/:id",
          method: "PUT",
        },

        // INTERNAL. Use Vehicle.state() instead.
        "::get::vehicle::state": {
          url: urlBase + "/vehicles/:id/state",
          method: "GET",
        },

        // INTERNAL. Use Site.state() instead.
        "::get::site::state": {
          url: urlBase + "/sites/:id/state",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.State.vehicles
     * @object
     * @description
     *
     * The object `State.vehicles` groups methods
     * manipulating `Vehicle` instances related to `State`.
     *
     * Use {@link lbServices.State#vehicles} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.State#vehicles
         * @methodOf lbServices.State
         *
         * @description
         *
         * Queries vehicles of state.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::get::state::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.vehicles#count
         * @methodOf lbServices.State.vehicles
         *
         * @description
         *
         * Counts vehicles of state.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.count = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::count::state::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.vehicles#create
         * @methodOf lbServices.State.vehicles
         *
         * @description
         *
         * Creates a new instance in vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.create = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::create::state::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.vehicles#destroyAll
         * @methodOf lbServices.State.vehicles
         *
         * @description
         *
         * Deletes all vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.vehicles.destroyAll = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::delete::state::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.vehicles#destroyById
         * @methodOf lbServices.State.vehicles
         *
         * @description
         *
         * Delete a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.vehicles.destroyById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::destroyById::state::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.vehicles#findById
         * @methodOf lbServices.State.vehicles
         *
         * @description
         *
         * Find a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.findById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::findById::state::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.vehicles#updateById
         * @methodOf lbServices.State.vehicles
         *
         * @description
         *
         * Update a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.updateById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::updateById::state::vehicles"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.State.sites
     * @object
     * @description
     *
     * The object `State.sites` groups methods
     * manipulating `Site` instances related to `State`.
     *
     * Use {@link lbServices.State#sites} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.State#sites
         * @methodOf lbServices.State
         *
         * @description
         *
         * Queries sites of state.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::state::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.sites#count
         * @methodOf lbServices.State.sites
         *
         * @description
         *
         * Counts sites of state.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.count = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::count::state::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.sites#create
         * @methodOf lbServices.State.sites
         *
         * @description
         *
         * Creates a new instance in sites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.create = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::create::state::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.sites#destroyAll
         * @methodOf lbServices.State.sites
         *
         * @description
         *
         * Deletes all sites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.sites.destroyAll = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::delete::state::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.sites#destroyById
         * @methodOf lbServices.State.sites
         *
         * @description
         *
         * Delete a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.sites.destroyById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::destroyById::state::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.sites#findById
         * @methodOf lbServices.State.sites
         *
         * @description
         *
         * Find a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.findById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::findById::state::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State.sites#updateById
         * @methodOf lbServices.State.sites
         *
         * @description
         *
         * Update a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.updateById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::updateById::state::sites"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.VehicleType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `VehicleType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "VehicleType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/vehicleTypes/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use VehicleType.vehicles.findById() instead.
        "prototype$__findById__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/:fk",
          method: "GET",
        },

        // INTERNAL. Use VehicleType.vehicles.destroyById() instead.
        "prototype$__destroyById__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use VehicleType.vehicles.updateById() instead.
        "prototype$__updateById__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/:fk",
          method: "PUT",
        },

        // INTERNAL. Use VehicleType.vehicles() instead.
        "prototype$__get__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use VehicleType.vehicles.create() instead.
        "prototype$__create__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use VehicleType.vehicles.destroyAll() instead.
        "prototype$__delete__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use VehicleType.vehicles.count() instead.
        "prototype$__count__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#create
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/vehicleTypes",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#upsert
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/vehicleTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#exists
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/vehicleTypes/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#findById
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/vehicleTypes/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#find
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/vehicleTypes",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#findOne
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/vehicleTypes/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#updateAll
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/vehicleTypes/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#deleteById
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/vehicleTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#count
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/vehicleTypes/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#prototype$updateAttributes
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/vehicleTypes/:id",
          method: "PUT",
        },

        // INTERNAL. Use Vehicle.vehicleType() instead.
        "::get::vehicle::vehicleType": {
          url: urlBase + "/vehicles/:id/vehicleType",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.VehicleType.vehicles
     * @object
     * @description
     *
     * The object `VehicleType.vehicles` groups methods
     * manipulating `Vehicle` instances related to `VehicleType`.
     *
     * Use {@link lbServices.VehicleType#vehicles} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.VehicleType#vehicles
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Queries vehicles of vehicleType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::get::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType.vehicles#count
         * @methodOf lbServices.VehicleType.vehicles
         *
         * @description
         *
         * Counts vehicles of vehicleType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.count = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::count::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType.vehicles#create
         * @methodOf lbServices.VehicleType.vehicles
         *
         * @description
         *
         * Creates a new instance in vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.create = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::create::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType.vehicles#destroyAll
         * @methodOf lbServices.VehicleType.vehicles
         *
         * @description
         *
         * Deletes all vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.vehicles.destroyAll = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::delete::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType.vehicles#destroyById
         * @methodOf lbServices.VehicleType.vehicles
         *
         * @description
         *
         * Delete a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.vehicles.destroyById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::destroyById::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType.vehicles#findById
         * @methodOf lbServices.VehicleType.vehicles
         *
         * @description
         *
         * Find a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.findById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::findById::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType.vehicles#updateById
         * @methodOf lbServices.VehicleType.vehicles
         *
         * @description
         *
         * Update a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.updateById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::updateById::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Vehicle
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Vehicle` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Vehicle",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/vehicles/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Vehicle.vehicleType() instead.
        "prototype$__get__vehicleType": {
          url: urlBase + "/vehicles/:id/vehicleType",
          method: "GET",
        },

        // INTERNAL. Use Vehicle.state() instead.
        "prototype$__get__state": {
          url: urlBase + "/vehicles/:id/state",
          method: "GET",
        },

        // INTERNAL. Use Vehicle.towReqs.findById() instead.
        "prototype$__findById__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/:fk",
          method: "GET",
        },

        // INTERNAL. Use Vehicle.towReqs.destroyById() instead.
        "prototype$__destroyById__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Vehicle.towReqs.updateById() instead.
        "prototype$__updateById__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Vehicle.transType() instead.
        "prototype$__get__transType": {
          url: urlBase + "/vehicles/:id/transType",
          method: "GET",
        },

        // INTERNAL. Use Vehicle.towReqs() instead.
        "prototype$__get__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Vehicle.towReqs.create() instead.
        "prototype$__create__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "POST",
        },

        // INTERNAL. Use Vehicle.towReqs.destroyAll() instead.
        "prototype$__delete__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use Vehicle.towReqs.count() instead.
        "prototype$__count__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#create
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/vehicles",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#upsert
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/vehicles",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#exists
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/vehicles/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#findById
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/vehicles/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#find
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/vehicles",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#findOne
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/vehicles/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#updateAll
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/vehicles/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#deleteById
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/vehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#count
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/vehicles/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#prototype$updateAttributes
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/vehicles/:id",
          method: "PUT",
        },

        // INTERNAL. Use State.vehicles.findById() instead.
        "::findById::state::vehicles": {
          url: urlBase + "/states/:id/vehicles/:fk",
          method: "GET",
        },

        // INTERNAL. Use State.vehicles.destroyById() instead.
        "::destroyById::state::vehicles": {
          url: urlBase + "/states/:id/vehicles/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use State.vehicles.updateById() instead.
        "::updateById::state::vehicles": {
          url: urlBase + "/states/:id/vehicles/:fk",
          method: "PUT",
        },

        // INTERNAL. Use State.vehicles() instead.
        "::get::state::vehicles": {
          url: urlBase + "/states/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use State.vehicles.create() instead.
        "::create::state::vehicles": {
          url: urlBase + "/states/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use State.vehicles.destroyAll() instead.
        "::delete::state::vehicles": {
          url: urlBase + "/states/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use State.vehicles.count() instead.
        "::count::state::vehicles": {
          url: urlBase + "/states/:id/vehicles/count",
          method: "GET",
        },

        // INTERNAL. Use VehicleType.vehicles.findById() instead.
        "::findById::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/:fk",
          method: "GET",
        },

        // INTERNAL. Use VehicleType.vehicles.destroyById() instead.
        "::destroyById::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use VehicleType.vehicles.updateById() instead.
        "::updateById::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/:fk",
          method: "PUT",
        },

        // INTERNAL. Use VehicleType.vehicles() instead.
        "::get::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use VehicleType.vehicles.create() instead.
        "::create::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use VehicleType.vehicles.destroyAll() instead.
        "::delete::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use VehicleType.vehicles.count() instead.
        "::count::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles/count",
          method: "GET",
        },

        // INTERNAL. Use TowReq.vehicle() instead.
        "::get::towReq::vehicle": {
          url: urlBase + "/towReqs/:id/vehicle",
          method: "GET",
        },

        // INTERNAL. Use TransType.vehicles.findById() instead.
        "::findById::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/:fk",
          method: "GET",
        },

        // INTERNAL. Use TransType.vehicles.destroyById() instead.
        "::destroyById::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use TransType.vehicles.updateById() instead.
        "::updateById::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/:fk",
          method: "PUT",
        },

        // INTERNAL. Use TransType.vehicles() instead.
        "::get::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use TransType.vehicles.create() instead.
        "::create::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use TransType.vehicles.destroyAll() instead.
        "::delete::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use TransType.vehicles.count() instead.
        "::count::transType::vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/count",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.Vehicle#vehicleType
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Fetches belongsTo relation vehicleType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        R.vehicleType = function() {
          var TargetResource = $injector.get("VehicleType");
          var action = TargetResource["::get::vehicle::vehicleType"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#state
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Fetches belongsTo relation state
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        R.state = function() {
          var TargetResource = $injector.get("State");
          var action = TargetResource["::get::vehicle::state"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Vehicle.towReqs
     * @object
     * @description
     *
     * The object `Vehicle.towReqs` groups methods
     * manipulating `TowReq` instances related to `Vehicle`.
     *
     * Use {@link lbServices.Vehicle#towReqs} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Vehicle#towReqs
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Queries towReqs of vehicle.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle.towReqs#count
         * @methodOf lbServices.Vehicle.towReqs
         *
         * @description
         *
         * Counts towReqs of vehicle.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.count = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::count::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle.towReqs#create
         * @methodOf lbServices.Vehicle.towReqs
         *
         * @description
         *
         * Creates a new instance in towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.create = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::create::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle.towReqs#destroyAll
         * @methodOf lbServices.Vehicle.towReqs
         *
         * @description
         *
         * Deletes all towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.towReqs.destroyAll = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::delete::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle.towReqs#destroyById
         * @methodOf lbServices.Vehicle.towReqs
         *
         * @description
         *
         * Delete a related item by id for towReqs
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqs
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.towReqs.destroyById = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::destroyById::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle.towReqs#findById
         * @methodOf lbServices.Vehicle.towReqs
         *
         * @description
         *
         * Find a related item by id for towReqs
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqs
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.findById = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::findById::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle.towReqs#updateById
         * @methodOf lbServices.Vehicle.towReqs
         *
         * @description
         *
         * Update a related item by id for towReqs
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqs
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.updateById = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::updateById::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#transType
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Fetches belongsTo relation transType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        R.transType = function() {
          var TargetResource = $injector.get("TransType");
          var action = TargetResource["::get::vehicle::transType"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Site
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Site` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Site",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/sites/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Site.state() instead.
        "prototype$__get__state": {
          url: urlBase + "/sites/:id/state",
          method: "GET",
        },

        // INTERNAL. Use Site.site() instead.
        "prototype$__get__site": {
          url: urlBase + "/sites/:id/site",
          method: "GET",
        },

        // INTERNAL. Use Site.sites.findById() instead.
        "prototype$__findById__sites": {
          url: urlBase + "/sites/:id/sites/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.sites.destroyById() instead.
        "prototype$__destroyById__sites": {
          url: urlBase + "/sites/:id/sites/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.sites.updateById() instead.
        "prototype$__updateById__sites": {
          url: urlBase + "/sites/:id/sites/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.parkingSigns.findById() instead.
        "prototype$__findById__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.parkingSigns.destroyById() instead.
        "prototype$__destroyById__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.parkingSigns.updateById() instead.
        "prototype$__updateById__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.siteGeos.findById() instead.
        "prototype$__findById__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.siteGeos.destroyById() instead.
        "prototype$__destroyById__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.siteGeos.updateById() instead.
        "prototype$__updateById__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.towReqs.findById() instead.
        "prototype$__findById__towReqs": {
          url: urlBase + "/sites/:id/towReqs/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.towReqs.destroyById() instead.
        "prototype$__destroyById__towReqs": {
          url: urlBase + "/sites/:id/towReqs/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.towReqs.updateById() instead.
        "prototype$__updateById__towReqs": {
          url: urlBase + "/sites/:id/towReqs/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.empDuties.findById() instead.
        "prototype$__findById__empDuties": {
          url: urlBase + "/sites/:id/empDuties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.empDuties.destroyById() instead.
        "prototype$__destroyById__empDuties": {
          url: urlBase + "/sites/:id/empDuties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.empDuties.updateById() instead.
        "prototype$__updateById__empDuties": {
          url: urlBase + "/sites/:id/empDuties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.sites() instead.
        "prototype$__get__sites": {
          url: urlBase + "/sites/:id/sites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.sites.create() instead.
        "prototype$__create__sites": {
          url: urlBase + "/sites/:id/sites",
          method: "POST",
        },

        // INTERNAL. Use Site.sites.destroyAll() instead.
        "prototype$__delete__sites": {
          url: urlBase + "/sites/:id/sites",
          method: "DELETE",
        },

        // INTERNAL. Use Site.sites.count() instead.
        "prototype$__count__sites": {
          url: urlBase + "/sites/:id/sites/count",
          method: "GET",
        },

        // INTERNAL. Use Site.parkingSigns() instead.
        "prototype$__get__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.parkingSigns.create() instead.
        "prototype$__create__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns",
          method: "POST",
        },

        // INTERNAL. Use Site.parkingSigns.destroyAll() instead.
        "prototype$__delete__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns",
          method: "DELETE",
        },

        // INTERNAL. Use Site.parkingSigns.count() instead.
        "prototype$__count__parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/count",
          method: "GET",
        },

        // INTERNAL. Use Site.siteGeos() instead.
        "prototype$__get__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.siteGeos.create() instead.
        "prototype$__create__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos",
          method: "POST",
        },

        // INTERNAL. Use Site.siteGeos.destroyAll() instead.
        "prototype$__delete__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos",
          method: "DELETE",
        },

        // INTERNAL. Use Site.siteGeos.count() instead.
        "prototype$__count__siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/count",
          method: "GET",
        },

        // INTERNAL. Use Site.towReqs() instead.
        "prototype$__get__towReqs": {
          url: urlBase + "/sites/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.towReqs.create() instead.
        "prototype$__create__towReqs": {
          url: urlBase + "/sites/:id/towReqs",
          method: "POST",
        },

        // INTERNAL. Use Site.towReqs.destroyAll() instead.
        "prototype$__delete__towReqs": {
          url: urlBase + "/sites/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use Site.towReqs.count() instead.
        "prototype$__count__towReqs": {
          url: urlBase + "/sites/:id/towReqs/count",
          method: "GET",
        },

        // INTERNAL. Use Site.empDuties() instead.
        "prototype$__get__empDuties": {
          url: urlBase + "/sites/:id/empDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.empDuties.create() instead.
        "prototype$__create__empDuties": {
          url: urlBase + "/sites/:id/empDuties",
          method: "POST",
        },

        // INTERNAL. Use Site.empDuties.destroyAll() instead.
        "prototype$__delete__empDuties": {
          url: urlBase + "/sites/:id/empDuties",
          method: "DELETE",
        },

        // INTERNAL. Use Site.empDuties.count() instead.
        "prototype$__count__empDuties": {
          url: urlBase + "/sites/:id/empDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#create
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/sites",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#upsert
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/sites",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#exists
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/sites/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#findById
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/sites/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#find
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/sites",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#findOne
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/sites/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#updateAll
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/sites/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#deleteById
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/sites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#count
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/sites/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Site#prototype$updateAttributes
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/sites/:id",
          method: "PUT",
        },

        // INTERNAL. Use State.sites.findById() instead.
        "::findById::state::sites": {
          url: urlBase + "/states/:id/sites/:fk",
          method: "GET",
        },

        // INTERNAL. Use State.sites.destroyById() instead.
        "::destroyById::state::sites": {
          url: urlBase + "/states/:id/sites/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use State.sites.updateById() instead.
        "::updateById::state::sites": {
          url: urlBase + "/states/:id/sites/:fk",
          method: "PUT",
        },

        // INTERNAL. Use State.sites() instead.
        "::get::state::sites": {
          url: urlBase + "/states/:id/sites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use State.sites.create() instead.
        "::create::state::sites": {
          url: urlBase + "/states/:id/sites",
          method: "POST",
        },

        // INTERNAL. Use State.sites.destroyAll() instead.
        "::delete::state::sites": {
          url: urlBase + "/states/:id/sites",
          method: "DELETE",
        },

        // INTERNAL. Use State.sites.count() instead.
        "::count::state::sites": {
          url: urlBase + "/states/:id/sites/count",
          method: "GET",
        },

        // INTERNAL. Use Site.site() instead.
        "::get::site::site": {
          url: urlBase + "/sites/:id/site",
          method: "GET",
        },

        // INTERNAL. Use Site.sites.findById() instead.
        "::findById::site::sites": {
          url: urlBase + "/sites/:id/sites/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.sites.destroyById() instead.
        "::destroyById::site::sites": {
          url: urlBase + "/sites/:id/sites/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.sites.updateById() instead.
        "::updateById::site::sites": {
          url: urlBase + "/sites/:id/sites/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.sites() instead.
        "::get::site::sites": {
          url: urlBase + "/sites/:id/sites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.sites.create() instead.
        "::create::site::sites": {
          url: urlBase + "/sites/:id/sites",
          method: "POST",
        },

        // INTERNAL. Use Site.sites.destroyAll() instead.
        "::delete::site::sites": {
          url: urlBase + "/sites/:id/sites",
          method: "DELETE",
        },

        // INTERNAL. Use Site.sites.count() instead.
        "::count::site::sites": {
          url: urlBase + "/sites/:id/sites/count",
          method: "GET",
        },

        // INTERNAL. Use ParkingSign.site() instead.
        "::get::parkingSign::site": {
          url: urlBase + "/parkingSigns/:id/site",
          method: "GET",
        },

        // INTERNAL. Use SiteGeo.site() instead.
        "::get::siteGeo::site": {
          url: urlBase + "/siteGeos/:id/site",
          method: "GET",
        },

        // INTERNAL. Use TowReq.site() instead.
        "::get::towReq::site": {
          url: urlBase + "/towReqs/:id/site",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.site() instead.
        "::get::empDuty::site": {
          url: urlBase + "/empDuties/:id/site",
          method: "GET",
        },

        // INTERNAL. Use Party.sites.findById() instead.
        "::findById::party::sites": {
          url: urlBase + "/parties/:id/sites/:fk",
          method: "GET",
        },

        // INTERNAL. Use Party.sites.destroyById() instead.
        "::destroyById::party::sites": {
          url: urlBase + "/parties/:id/sites/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Party.sites.updateById() instead.
        "::updateById::party::sites": {
          url: urlBase + "/parties/:id/sites/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Party.sites() instead.
        "::get::party::sites": {
          url: urlBase + "/parties/:id/sites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Party.sites.create() instead.
        "::create::party::sites": {
          url: urlBase + "/parties/:id/sites",
          method: "POST",
        },

        // INTERNAL. Use Party.sites.destroyAll() instead.
        "::delete::party::sites": {
          url: urlBase + "/parties/:id/sites",
          method: "DELETE",
        },

        // INTERNAL. Use Party.sites.count() instead.
        "::count::party::sites": {
          url: urlBase + "/parties/:id/sites/count",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.Site#state
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Fetches belongsTo relation state
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        R.state = function() {
          var TargetResource = $injector.get("State");
          var action = TargetResource["::get::site::state"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site#site
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Fetches belongsTo relation site
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.site = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::site::site"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Site.sites
     * @object
     * @description
     *
     * The object `Site.sites` groups methods
     * manipulating `Site` instances related to `Site`.
     *
     * Use {@link lbServices.Site#sites} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Site#sites
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Queries sites of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::site::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.sites#count
         * @methodOf lbServices.Site.sites
         *
         * @description
         *
         * Counts sites of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.count = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::count::site::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.sites#create
         * @methodOf lbServices.Site.sites
         *
         * @description
         *
         * Creates a new instance in sites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.create = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::create::site::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.sites#destroyAll
         * @methodOf lbServices.Site.sites
         *
         * @description
         *
         * Deletes all sites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.sites.destroyAll = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::delete::site::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.sites#destroyById
         * @methodOf lbServices.Site.sites
         *
         * @description
         *
         * Delete a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.sites.destroyById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::destroyById::site::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.sites#findById
         * @methodOf lbServices.Site.sites
         *
         * @description
         *
         * Find a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.findById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::findById::site::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.sites#updateById
         * @methodOf lbServices.Site.sites
         *
         * @description
         *
         * Update a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.updateById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::updateById::site::sites"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Site.parkingSigns
     * @object
     * @description
     *
     * The object `Site.parkingSigns` groups methods
     * manipulating `ParkingSign` instances related to `Site`.
     *
     * Use {@link lbServices.Site#parkingSigns} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Site#parkingSigns
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Queries parkingSigns of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::get::site::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.parkingSigns#count
         * @methodOf lbServices.Site.parkingSigns
         *
         * @description
         *
         * Counts parkingSigns of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns.count = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::count::site::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.parkingSigns#create
         * @methodOf lbServices.Site.parkingSigns
         *
         * @description
         *
         * Creates a new instance in parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns.create = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::create::site::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.parkingSigns#destroyAll
         * @methodOf lbServices.Site.parkingSigns
         *
         * @description
         *
         * Deletes all parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.parkingSigns.destroyAll = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::delete::site::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.parkingSigns#destroyById
         * @methodOf lbServices.Site.parkingSigns
         *
         * @description
         *
         * Delete a related item by id for parkingSigns
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for parkingSigns
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.parkingSigns.destroyById = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::destroyById::site::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.parkingSigns#findById
         * @methodOf lbServices.Site.parkingSigns
         *
         * @description
         *
         * Find a related item by id for parkingSigns
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for parkingSigns
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns.findById = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::findById::site::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.parkingSigns#updateById
         * @methodOf lbServices.Site.parkingSigns
         *
         * @description
         *
         * Update a related item by id for parkingSigns
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for parkingSigns
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns.updateById = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::updateById::site::parkingSigns"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Site.siteGeos
     * @object
     * @description
     *
     * The object `Site.siteGeos` groups methods
     * manipulating `SiteGeo` instances related to `Site`.
     *
     * Use {@link lbServices.Site#siteGeos} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Site#siteGeos
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Queries siteGeos of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::get::site::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.siteGeos#count
         * @methodOf lbServices.Site.siteGeos
         *
         * @description
         *
         * Counts siteGeos of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.count = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::count::site::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.siteGeos#create
         * @methodOf lbServices.Site.siteGeos
         *
         * @description
         *
         * Creates a new instance in siteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.create = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::create::site::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.siteGeos#destroyAll
         * @methodOf lbServices.Site.siteGeos
         *
         * @description
         *
         * Deletes all siteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.siteGeos.destroyAll = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::delete::site::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.siteGeos#destroyById
         * @methodOf lbServices.Site.siteGeos
         *
         * @description
         *
         * Delete a related item by id for siteGeos
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for siteGeos
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.siteGeos.destroyById = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::destroyById::site::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.siteGeos#findById
         * @methodOf lbServices.Site.siteGeos
         *
         * @description
         *
         * Find a related item by id for siteGeos
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for siteGeos
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.findById = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::findById::site::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.siteGeos#updateById
         * @methodOf lbServices.Site.siteGeos
         *
         * @description
         *
         * Update a related item by id for siteGeos
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for siteGeos
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.updateById = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::updateById::site::siteGeos"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Site.towReqs
     * @object
     * @description
     *
     * The object `Site.towReqs` groups methods
     * manipulating `TowReq` instances related to `Site`.
     *
     * Use {@link lbServices.Site#towReqs} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Site#towReqs
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Queries towReqs of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::site::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.towReqs#count
         * @methodOf lbServices.Site.towReqs
         *
         * @description
         *
         * Counts towReqs of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.count = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::count::site::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.towReqs#create
         * @methodOf lbServices.Site.towReqs
         *
         * @description
         *
         * Creates a new instance in towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.create = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::create::site::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.towReqs#destroyAll
         * @methodOf lbServices.Site.towReqs
         *
         * @description
         *
         * Deletes all towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.towReqs.destroyAll = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::delete::site::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.towReqs#destroyById
         * @methodOf lbServices.Site.towReqs
         *
         * @description
         *
         * Delete a related item by id for towReqs
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqs
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.towReqs.destroyById = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::destroyById::site::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.towReqs#findById
         * @methodOf lbServices.Site.towReqs
         *
         * @description
         *
         * Find a related item by id for towReqs
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqs
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.findById = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::findById::site::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.towReqs#updateById
         * @methodOf lbServices.Site.towReqs
         *
         * @description
         *
         * Update a related item by id for towReqs
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqs
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.updateById = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::updateById::site::towReqs"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Site.empDuties
     * @object
     * @description
     *
     * The object `Site.empDuties` groups methods
     * manipulating `EmpDuty` instances related to `Site`.
     *
     * Use {@link lbServices.Site#empDuties} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Site#empDuties
         * @methodOf lbServices.Site
         *
         * @description
         *
         * Queries empDuties of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::get::site::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.empDuties#count
         * @methodOf lbServices.Site.empDuties
         *
         * @description
         *
         * Counts empDuties of site.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.count = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::count::site::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.empDuties#create
         * @methodOf lbServices.Site.empDuties
         *
         * @description
         *
         * Creates a new instance in empDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.create = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::create::site::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.empDuties#destroyAll
         * @methodOf lbServices.Site.empDuties
         *
         * @description
         *
         * Deletes all empDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.empDuties.destroyAll = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::delete::site::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.empDuties#destroyById
         * @methodOf lbServices.Site.empDuties
         *
         * @description
         *
         * Delete a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.empDuties.destroyById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::destroyById::site::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.empDuties#findById
         * @methodOf lbServices.Site.empDuties
         *
         * @description
         *
         * Find a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.findById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::findById::site::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Site.empDuties#updateById
         * @methodOf lbServices.Site.empDuties
         *
         * @description
         *
         * Update a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.updateById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::updateById::site::empDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ParkingSign
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ParkingSign` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ParkingSign",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/parkingSigns/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use ParkingSign.site() instead.
        "prototype$__get__site": {
          url: urlBase + "/parkingSigns/:id/site",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#create
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/parkingSigns",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#upsert
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/parkingSigns",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#exists
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/parkingSigns/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#findById
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/parkingSigns/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#find
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/parkingSigns",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#findOne
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/parkingSigns/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#updateAll
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/parkingSigns/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#deleteById
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/parkingSigns/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#count
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/parkingSigns/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#prototype$updateAttributes
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/parkingSigns/:id",
          method: "PUT",
        },

        // INTERNAL. Use Site.parkingSigns.findById() instead.
        "::findById::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.parkingSigns.destroyById() instead.
        "::destroyById::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.parkingSigns.updateById() instead.
        "::updateById::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.parkingSigns() instead.
        "::get::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.parkingSigns.create() instead.
        "::create::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns",
          method: "POST",
        },

        // INTERNAL. Use Site.parkingSigns.destroyAll() instead.
        "::delete::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns",
          method: "DELETE",
        },

        // INTERNAL. Use Site.parkingSigns.count() instead.
        "::count::site::parkingSigns": {
          url: urlBase + "/sites/:id/parkingSigns/count",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#site
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Fetches belongsTo relation site
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.site = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::parkingSign::site"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.GeoType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `GeoType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "GeoType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/geoTypes/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use GeoType.siteGeos.findById() instead.
        "prototype$__findById__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/:fk",
          method: "GET",
        },

        // INTERNAL. Use GeoType.siteGeos.destroyById() instead.
        "prototype$__destroyById__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use GeoType.siteGeos.updateById() instead.
        "prototype$__updateById__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/:fk",
          method: "PUT",
        },

        // INTERNAL. Use GeoType.siteGeos() instead.
        "prototype$__get__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use GeoType.siteGeos.create() instead.
        "prototype$__create__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos",
          method: "POST",
        },

        // INTERNAL. Use GeoType.siteGeos.destroyAll() instead.
        "prototype$__delete__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos",
          method: "DELETE",
        },

        // INTERNAL. Use GeoType.siteGeos.count() instead.
        "prototype$__count__siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#create
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/geoTypes",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#upsert
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/geoTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#exists
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/geoTypes/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#findById
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/geoTypes/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#find
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/geoTypes",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#findOne
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/geoTypes/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#updateAll
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/geoTypes/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#deleteById
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/geoTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#count
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/geoTypes/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.GeoType#prototype$updateAttributes
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/geoTypes/:id",
          method: "PUT",
        },

        // INTERNAL. Use SiteGeo.geoType() instead.
        "::get::siteGeo::geoType": {
          url: urlBase + "/siteGeos/:id/geoType",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.GeoType.siteGeos
     * @object
     * @description
     *
     * The object `GeoType.siteGeos` groups methods
     * manipulating `SiteGeo` instances related to `GeoType`.
     *
     * Use {@link lbServices.GeoType#siteGeos} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.GeoType#siteGeos
         * @methodOf lbServices.GeoType
         *
         * @description
         *
         * Queries siteGeos of geoType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::get::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.GeoType.siteGeos#count
         * @methodOf lbServices.GeoType.siteGeos
         *
         * @description
         *
         * Counts siteGeos of geoType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.count = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::count::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.GeoType.siteGeos#create
         * @methodOf lbServices.GeoType.siteGeos
         *
         * @description
         *
         * Creates a new instance in siteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.create = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::create::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.GeoType.siteGeos#destroyAll
         * @methodOf lbServices.GeoType.siteGeos
         *
         * @description
         *
         * Deletes all siteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.siteGeos.destroyAll = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::delete::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.GeoType.siteGeos#destroyById
         * @methodOf lbServices.GeoType.siteGeos
         *
         * @description
         *
         * Delete a related item by id for siteGeos
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for siteGeos
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.siteGeos.destroyById = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::destroyById::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.GeoType.siteGeos#findById
         * @methodOf lbServices.GeoType.siteGeos
         *
         * @description
         *
         * Find a related item by id for siteGeos
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for siteGeos
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.findById = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::findById::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.GeoType.siteGeos#updateById
         * @methodOf lbServices.GeoType.siteGeos
         *
         * @description
         *
         * Update a related item by id for siteGeos
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for siteGeos
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        R.siteGeos.updateById = function() {
          var TargetResource = $injector.get("SiteGeo");
          var action = TargetResource["::updateById::geoType::siteGeos"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.SiteGeo
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `SiteGeo` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "SiteGeo",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/siteGeos/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use SiteGeo.site() instead.
        "prototype$__get__site": {
          url: urlBase + "/siteGeos/:id/site",
          method: "GET",
        },

        // INTERNAL. Use SiteGeo.geoType() instead.
        "prototype$__get__geoType": {
          url: urlBase + "/siteGeos/:id/geoType",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#create
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/siteGeos",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#upsert
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/siteGeos",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#exists
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/siteGeos/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#findById
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/siteGeos/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#find
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/siteGeos",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#findOne
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/siteGeos/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#updateAll
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/siteGeos/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#deleteById
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/siteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#count
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/siteGeos/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#prototype$updateAttributes
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SiteGeo` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/siteGeos/:id",
          method: "PUT",
        },

        // INTERNAL. Use Site.siteGeos.findById() instead.
        "::findById::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.siteGeos.destroyById() instead.
        "::destroyById::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.siteGeos.updateById() instead.
        "::updateById::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.siteGeos() instead.
        "::get::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.siteGeos.create() instead.
        "::create::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos",
          method: "POST",
        },

        // INTERNAL. Use Site.siteGeos.destroyAll() instead.
        "::delete::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos",
          method: "DELETE",
        },

        // INTERNAL. Use Site.siteGeos.count() instead.
        "::count::site::siteGeos": {
          url: urlBase + "/sites/:id/siteGeos/count",
          method: "GET",
        },

        // INTERNAL. Use GeoType.siteGeos.findById() instead.
        "::findById::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/:fk",
          method: "GET",
        },

        // INTERNAL. Use GeoType.siteGeos.destroyById() instead.
        "::destroyById::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use GeoType.siteGeos.updateById() instead.
        "::updateById::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/:fk",
          method: "PUT",
        },

        // INTERNAL. Use GeoType.siteGeos() instead.
        "::get::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use GeoType.siteGeos.create() instead.
        "::create::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos",
          method: "POST",
        },

        // INTERNAL. Use GeoType.siteGeos.destroyAll() instead.
        "::delete::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos",
          method: "DELETE",
        },

        // INTERNAL. Use GeoType.siteGeos.count() instead.
        "::count::geoType::siteGeos": {
          url: urlBase + "/geoTypes/:id/siteGeos/count",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#site
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Fetches belongsTo relation site
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.site = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::siteGeo::site"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.SiteGeo#geoType
         * @methodOf lbServices.SiteGeo
         *
         * @description
         *
         * Fetches belongsTo relation geoType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `GeoType` object.)
         * </em>
         */
        R.geoType = function() {
          var TargetResource = $injector.get("GeoType");
          var action = TargetResource["::get::siteGeo::geoType"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowReq
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowReq` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowReq",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towReqs/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use TowReq.vehicle() instead.
        "prototype$__get__vehicle": {
          url: urlBase + "/towReqs/:id/vehicle",
          method: "GET",
        },

        // INTERNAL. Use TowReq.site() instead.
        "prototype$__get__site": {
          url: urlBase + "/towReqs/:id/site",
          method: "GET",
        },

        // INTERNAL. Use TowReq.statusEntries.findById() instead.
        "prototype$__findById__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/:fk",
          method: "GET",
        },

        // INTERNAL. Use TowReq.statusEntries.destroyById() instead.
        "prototype$__destroyById__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use TowReq.statusEntries.updateById() instead.
        "prototype$__updateById__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/:fk",
          method: "PUT",
        },

        // INTERNAL. Use TowReq.statusEntries() instead.
        "prototype$__get__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use TowReq.statusEntries.create() instead.
        "prototype$__create__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use TowReq.statusEntries.destroyAll() instead.
        "prototype$__delete__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use TowReq.statusEntries.count() instead.
        "prototype$__count__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#create
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towReqs",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#upsert
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towReqs",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#exists
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/towReqs/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#findById
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towReqs/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#find
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towReqs",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#findOne
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towReqs/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#updateAll
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/towReqs/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#deleteById
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towReqs/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#count
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towReqs/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$updateAttributes
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towReqs/:id",
          method: "PUT",
        },

        // INTERNAL. Use Vehicle.towReqs.findById() instead.
        "::findById::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/:fk",
          method: "GET",
        },

        // INTERNAL. Use Vehicle.towReqs.destroyById() instead.
        "::destroyById::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Vehicle.towReqs.updateById() instead.
        "::updateById::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Vehicle.towReqs() instead.
        "::get::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Vehicle.towReqs.create() instead.
        "::create::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "POST",
        },

        // INTERNAL. Use Vehicle.towReqs.destroyAll() instead.
        "::delete::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use Vehicle.towReqs.count() instead.
        "::count::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs/count",
          method: "GET",
        },

        // INTERNAL. Use Site.towReqs.findById() instead.
        "::findById::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.towReqs.destroyById() instead.
        "::destroyById::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.towReqs.updateById() instead.
        "::updateById::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.towReqs() instead.
        "::get::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.towReqs.create() instead.
        "::create::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs",
          method: "POST",
        },

        // INTERNAL. Use Site.towReqs.destroyAll() instead.
        "::delete::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use Site.towReqs.count() instead.
        "::count::site::towReqs": {
          url: urlBase + "/sites/:id/towReqs/count",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.towReq() instead.
        "::get::statusEntry::towReq": {
          url: urlBase + "/statusEntries/:id/towReq",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.TowReq#vehicle
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Fetches belongsTo relation vehicle
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicle = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::get::towReq::vehicle"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#site
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Fetches belongsTo relation site
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.site = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::towReq::site"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.TowReq.statusEntries
     * @object
     * @description
     *
     * The object `TowReq.statusEntries` groups methods
     * manipulating `StatusEntry` instances related to `TowReq`.
     *
     * Use {@link lbServices.TowReq#statusEntries} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.TowReq#statusEntries
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Queries statusEntries of towReq.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq.statusEntries#count
         * @methodOf lbServices.TowReq.statusEntries
         *
         * @description
         *
         * Counts statusEntries of towReq.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.count = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::count::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq.statusEntries#create
         * @methodOf lbServices.TowReq.statusEntries
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq.statusEntries#destroyAll
         * @methodOf lbServices.TowReq.statusEntries
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq.statusEntries#destroyById
         * @methodOf lbServices.TowReq.statusEntries
         *
         * @description
         *
         * Delete a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.statusEntries.destroyById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::destroyById::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq.statusEntries#findById
         * @methodOf lbServices.TowReq.statusEntries
         *
         * @description
         *
         * Find a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.findById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::findById::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq.statusEntries#updateById
         * @methodOf lbServices.TowReq.statusEntries
         *
         * @description
         *
         * Update a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.updateById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::updateById::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Duty
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Duty` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Duty",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqDuties/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Duty.empDuties.findById() instead.
        "prototype$__findById__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Duty.empDuties.destroyById() instead.
        "prototype$__destroyById__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Duty.empDuties.updateById() instead.
        "prototype$__updateById__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Duty.party() instead.
        "prototype$__get__party": {
          url: urlBase + "/reqDuties/:id/party",
          method: "GET",
        },

        // INTERNAL. Use Duty.empDuties() instead.
        "prototype$__get__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Duty.empDuties.create() instead.
        "prototype$__create__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties",
          method: "POST",
        },

        // INTERNAL. Use Duty.empDuties.destroyAll() instead.
        "prototype$__delete__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties",
          method: "DELETE",
        },

        // INTERNAL. Use Duty.empDuties.count() instead.
        "prototype$__count__empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#create
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#upsert
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#exists
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/reqDuties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#findById
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqDuties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#find
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#findOne
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqDuties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#updateAll
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/reqDuties/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#deleteById
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#count
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Duty#prototype$updateAttributes
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqDuties/:id",
          method: "PUT",
        },

        // INTERNAL. Use EmpDuty.duty() instead.
        "::get::empDuty::duty": {
          url: urlBase + "/empDuties/:id/duty",
          method: "GET",
        },

        // INTERNAL. Use Party.duties.findById() instead.
        "::findById::party::duties": {
          url: urlBase + "/parties/:id/duties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Party.duties.destroyById() instead.
        "::destroyById::party::duties": {
          url: urlBase + "/parties/:id/duties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Party.duties.updateById() instead.
        "::updateById::party::duties": {
          url: urlBase + "/parties/:id/duties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Party.duties() instead.
        "::get::party::duties": {
          url: urlBase + "/parties/:id/duties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Party.duties.create() instead.
        "::create::party::duties": {
          url: urlBase + "/parties/:id/duties",
          method: "POST",
        },

        // INTERNAL. Use Party.duties.destroyAll() instead.
        "::delete::party::duties": {
          url: urlBase + "/parties/:id/duties",
          method: "DELETE",
        },

        // INTERNAL. Use Party.duties.count() instead.
        "::count::party::duties": {
          url: urlBase + "/parties/:id/duties/count",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.Duty.empDuties
     * @object
     * @description
     *
     * The object `Duty.empDuties` groups methods
     * manipulating `EmpDuty` instances related to `Duty`.
     *
     * Use {@link lbServices.Duty#empDuties} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Duty#empDuties
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Queries empDuties of duty.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::get::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty.empDuties#count
         * @methodOf lbServices.Duty.empDuties
         *
         * @description
         *
         * Counts empDuties of duty.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.count = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::count::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty.empDuties#create
         * @methodOf lbServices.Duty.empDuties
         *
         * @description
         *
         * Creates a new instance in empDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.create = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::create::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty.empDuties#destroyAll
         * @methodOf lbServices.Duty.empDuties
         *
         * @description
         *
         * Deletes all empDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.empDuties.destroyAll = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::delete::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty.empDuties#destroyById
         * @methodOf lbServices.Duty.empDuties
         *
         * @description
         *
         * Delete a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.empDuties.destroyById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::destroyById::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty.empDuties#findById
         * @methodOf lbServices.Duty.empDuties
         *
         * @description
         *
         * Find a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.findById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::findById::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty.empDuties#updateById
         * @methodOf lbServices.Duty.empDuties
         *
         * @description
         *
         * Update a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.updateById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::updateById::duty::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Duty#party
         * @methodOf lbServices.Duty
         *
         * @description
         *
         * Fetches belongsTo relation party
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        R.party = function() {
          var TargetResource = $injector.get("Party");
          var action = TargetResource["::get::duty::party"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Emp
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Emp` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Emp",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/emps/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Emp.empDuties.findById() instead.
        "prototype$__findById__empDuties": {
          url: urlBase + "/emps/:id/empDuties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Emp.empDuties.destroyById() instead.
        "prototype$__destroyById__empDuties": {
          url: urlBase + "/emps/:id/empDuties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Emp.empDuties.updateById() instead.
        "prototype$__updateById__empDuties": {
          url: urlBase + "/emps/:id/empDuties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Emp.empDuties() instead.
        "prototype$__get__empDuties": {
          url: urlBase + "/emps/:id/empDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Emp.empDuties.create() instead.
        "prototype$__create__empDuties": {
          url: urlBase + "/emps/:id/empDuties",
          method: "POST",
        },

        // INTERNAL. Use Emp.empDuties.destroyAll() instead.
        "prototype$__delete__empDuties": {
          url: urlBase + "/emps/:id/empDuties",
          method: "DELETE",
        },

        // INTERNAL. Use Emp.empDuties.count() instead.
        "prototype$__count__empDuties": {
          url: urlBase + "/emps/:id/empDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#create
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/emps",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#upsert
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/emps",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#exists
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/emps/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#findById
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/emps/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#find
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/emps",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#findOne
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/emps/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#updateAll
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/emps/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#deleteById
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/emps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#count
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/emps/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Emp#prototype$updateAttributes
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/emps/:id",
          method: "PUT",
        },

        // INTERNAL. Use EmpDuty.emp() instead.
        "::get::empDuty::emp": {
          url: urlBase + "/empDuties/:id/emp",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.Emp.empDuties
     * @object
     * @description
     *
     * The object `Emp.empDuties` groups methods
     * manipulating `EmpDuty` instances related to `Emp`.
     *
     * Use {@link lbServices.Emp#empDuties} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Emp#empDuties
         * @methodOf lbServices.Emp
         *
         * @description
         *
         * Queries empDuties of emp.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::get::emp::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Emp.empDuties#count
         * @methodOf lbServices.Emp.empDuties
         *
         * @description
         *
         * Counts empDuties of emp.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.count = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::count::emp::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Emp.empDuties#create
         * @methodOf lbServices.Emp.empDuties
         *
         * @description
         *
         * Creates a new instance in empDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.create = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::create::emp::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Emp.empDuties#destroyAll
         * @methodOf lbServices.Emp.empDuties
         *
         * @description
         *
         * Deletes all empDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.empDuties.destroyAll = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::delete::emp::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Emp.empDuties#destroyById
         * @methodOf lbServices.Emp.empDuties
         *
         * @description
         *
         * Delete a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.empDuties.destroyById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::destroyById::emp::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Emp.empDuties#findById
         * @methodOf lbServices.Emp.empDuties
         *
         * @description
         *
         * Find a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.findById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::findById::emp::empDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Emp.empDuties#updateById
         * @methodOf lbServices.Emp.empDuties
         *
         * @description
         *
         * Update a related item by id for empDuties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for empDuties
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuties.updateById = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::updateById::emp::empDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.EmpDuty
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `EmpDuty` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "EmpDuty",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/empDuties/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use EmpDuty.site() instead.
        "prototype$__get__site": {
          url: urlBase + "/empDuties/:id/site",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.duty() instead.
        "prototype$__get__duty": {
          url: urlBase + "/empDuties/:id/duty",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.emp() instead.
        "prototype$__get__emp": {
          url: urlBase + "/empDuties/:id/emp",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.statusEntries.findById() instead.
        "prototype$__findById__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/:fk",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.statusEntries.destroyById() instead.
        "prototype$__destroyById__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use EmpDuty.statusEntries.updateById() instead.
        "prototype$__updateById__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/:fk",
          method: "PUT",
        },

        // INTERNAL. Use EmpDuty.statusEntries() instead.
        "prototype$__get__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use EmpDuty.statusEntries.create() instead.
        "prototype$__create__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use EmpDuty.statusEntries.destroyAll() instead.
        "prototype$__delete__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use EmpDuty.statusEntries.count() instead.
        "prototype$__count__statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#create
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/empDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#upsert
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/empDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#exists
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/empDuties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#findById
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/empDuties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#find
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/empDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#findOne
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/empDuties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#updateAll
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/empDuties/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#deleteById
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/empDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#count
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/empDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#prototype$updateAttributes
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/empDuties/:id",
          method: "PUT",
        },

        // INTERNAL. Use Site.empDuties.findById() instead.
        "::findById::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Site.empDuties.destroyById() instead.
        "::destroyById::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Site.empDuties.updateById() instead.
        "::updateById::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Site.empDuties() instead.
        "::get::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Site.empDuties.create() instead.
        "::create::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties",
          method: "POST",
        },

        // INTERNAL. Use Site.empDuties.destroyAll() instead.
        "::delete::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties",
          method: "DELETE",
        },

        // INTERNAL. Use Site.empDuties.count() instead.
        "::count::site::empDuties": {
          url: urlBase + "/sites/:id/empDuties/count",
          method: "GET",
        },

        // INTERNAL. Use Duty.empDuties.findById() instead.
        "::findById::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Duty.empDuties.destroyById() instead.
        "::destroyById::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Duty.empDuties.updateById() instead.
        "::updateById::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Duty.empDuties() instead.
        "::get::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Duty.empDuties.create() instead.
        "::create::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties",
          method: "POST",
        },

        // INTERNAL. Use Duty.empDuties.destroyAll() instead.
        "::delete::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties",
          method: "DELETE",
        },

        // INTERNAL. Use Duty.empDuties.count() instead.
        "::count::duty::empDuties": {
          url: urlBase + "/reqDuties/:id/empDuties/count",
          method: "GET",
        },

        // INTERNAL. Use Emp.empDuties.findById() instead.
        "::findById::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Emp.empDuties.destroyById() instead.
        "::destroyById::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Emp.empDuties.updateById() instead.
        "::updateById::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Emp.empDuties() instead.
        "::get::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Emp.empDuties.create() instead.
        "::create::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties",
          method: "POST",
        },

        // INTERNAL. Use Emp.empDuties.destroyAll() instead.
        "::delete::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties",
          method: "DELETE",
        },

        // INTERNAL. Use Emp.empDuties.count() instead.
        "::count::emp::empDuties": {
          url: urlBase + "/emps/:id/empDuties/count",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.empDuty() instead.
        "::get::statusEntry::empDuty": {
          url: urlBase + "/statusEntries/:id/empDuty",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#site
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Fetches belongsTo relation site
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.site = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::empDuty::site"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#duty
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Fetches belongsTo relation duty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        R.duty = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::get::empDuty::duty"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#emp
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Fetches belongsTo relation emp
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Emp` object.)
         * </em>
         */
        R.emp = function() {
          var TargetResource = $injector.get("Emp");
          var action = TargetResource["::get::empDuty::emp"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.EmpDuty.statusEntries
     * @object
     * @description
     *
     * The object `EmpDuty.statusEntries` groups methods
     * manipulating `StatusEntry` instances related to `EmpDuty`.
     *
     * Use {@link lbServices.EmpDuty#statusEntries} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.EmpDuty#statusEntries
         * @methodOf lbServices.EmpDuty
         *
         * @description
         *
         * Queries statusEntries of empDuty.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty.statusEntries#count
         * @methodOf lbServices.EmpDuty.statusEntries
         *
         * @description
         *
         * Counts statusEntries of empDuty.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.count = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::count::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty.statusEntries#create
         * @methodOf lbServices.EmpDuty.statusEntries
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty.statusEntries#destroyAll
         * @methodOf lbServices.EmpDuty.statusEntries
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty.statusEntries#destroyById
         * @methodOf lbServices.EmpDuty.statusEntries
         *
         * @description
         *
         * Delete a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.statusEntries.destroyById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::destroyById::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty.statusEntries#findById
         * @methodOf lbServices.EmpDuty.statusEntries
         *
         * @description
         *
         * Find a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.findById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::findById::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.EmpDuty.statusEntries#updateById
         * @methodOf lbServices.EmpDuty.statusEntries
         *
         * @description
         *
         * Update a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.updateById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::updateById::empDuty::statusEntries"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Status
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Status` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Status",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/statuses/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Status.statusEntries.findById() instead.
        "prototype$__findById__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/:fk",
          method: "GET",
        },

        // INTERNAL. Use Status.statusEntries.destroyById() instead.
        "prototype$__destroyById__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Status.statusEntries.updateById() instead.
        "prototype$__updateById__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Status.statusEntries() instead.
        "prototype$__get__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Status.statusEntries.create() instead.
        "prototype$__create__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use Status.statusEntries.destroyAll() instead.
        "prototype$__delete__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use Status.statusEntries.count() instead.
        "prototype$__count__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#create
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/statuses",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#upsert
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/statuses",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#exists
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/statuses/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#findById
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/statuses/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#find
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/statuses",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#findOne
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/statuses/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#updateAll
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/statuses/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#deleteById
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/statuses/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#count
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/statuses/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#prototype$updateAttributes
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/statuses/:id",
          method: "PUT",
        },

        // INTERNAL. Use StatusEntry.status() instead.
        "::get::statusEntry::status": {
          url: urlBase + "/statusEntries/:id/status",
          method: "GET",
        },

        // INTERNAL. Use StatusRight.status() instead.
        "::get::statusRight::status": {
          url: urlBase + "/statusRights/:id/status",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.Status.statusEntries
     * @object
     * @description
     *
     * The object `Status.statusEntries` groups methods
     * manipulating `StatusEntry` instances related to `Status`.
     *
     * Use {@link lbServices.Status#statusEntries} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Status#statusEntries
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Queries statusEntries of status.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status.statusEntries#count
         * @methodOf lbServices.Status.statusEntries
         *
         * @description
         *
         * Counts statusEntries of status.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.count = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::count::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status.statusEntries#create
         * @methodOf lbServices.Status.statusEntries
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status.statusEntries#destroyAll
         * @methodOf lbServices.Status.statusEntries
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status.statusEntries#destroyById
         * @methodOf lbServices.Status.statusEntries
         *
         * @description
         *
         * Delete a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.statusEntries.destroyById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::destroyById::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status.statusEntries#findById
         * @methodOf lbServices.Status.statusEntries
         *
         * @description
         *
         * Find a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.findById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::findById::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status.statusEntries#updateById
         * @methodOf lbServices.Status.statusEntries
         *
         * @description
         *
         * Update a related item by id for statusEntries
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for statusEntries
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.updateById = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::updateById::status::statusEntries"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.StatusEntry
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `StatusEntry` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "StatusEntry",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/statusEntries/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use StatusEntry.empDuty() instead.
        "prototype$__get__empDuty": {
          url: urlBase + "/statusEntries/:id/empDuty",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.status() instead.
        "prototype$__get__status": {
          url: urlBase + "/statusEntries/:id/status",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.towReq() instead.
        "prototype$__get__towReq": {
          url: urlBase + "/statusEntries/:id/towReq",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.towReqPics.findById() instead.
        "prototype$__findById__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/:fk",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.towReqPics.destroyById() instead.
        "prototype$__destroyById__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use StatusEntry.towReqPics.updateById() instead.
        "prototype$__updateById__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/:fk",
          method: "PUT",
        },

        // INTERNAL. Use StatusEntry.towReqPics() instead.
        "prototype$__get__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use StatusEntry.towReqPics.create() instead.
        "prototype$__create__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics",
          method: "POST",
        },

        // INTERNAL. Use StatusEntry.towReqPics.destroyAll() instead.
        "prototype$__delete__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics",
          method: "DELETE",
        },

        // INTERNAL. Use StatusEntry.towReqPics.count() instead.
        "prototype$__count__towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#create
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/statusEntries",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#upsert
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/statusEntries",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#exists
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/statusEntries/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#findById
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/statusEntries/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#find
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/statusEntries",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#findOne
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/statusEntries/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#updateAll
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/statusEntries/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#deleteById
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/statusEntries/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#count
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/statusEntries/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#prototype$updateAttributes
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/statusEntries/:id",
          method: "PUT",
        },

        // INTERNAL. Use TowReq.statusEntries.findById() instead.
        "::findById::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/:fk",
          method: "GET",
        },

        // INTERNAL. Use TowReq.statusEntries.destroyById() instead.
        "::destroyById::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use TowReq.statusEntries.updateById() instead.
        "::updateById::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/:fk",
          method: "PUT",
        },

        // INTERNAL. Use TowReq.statusEntries() instead.
        "::get::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use TowReq.statusEntries.create() instead.
        "::create::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use TowReq.statusEntries.destroyAll() instead.
        "::delete::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use TowReq.statusEntries.count() instead.
        "::count::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries/count",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.statusEntries.findById() instead.
        "::findById::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/:fk",
          method: "GET",
        },

        // INTERNAL. Use EmpDuty.statusEntries.destroyById() instead.
        "::destroyById::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use EmpDuty.statusEntries.updateById() instead.
        "::updateById::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/:fk",
          method: "PUT",
        },

        // INTERNAL. Use EmpDuty.statusEntries() instead.
        "::get::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use EmpDuty.statusEntries.create() instead.
        "::create::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use EmpDuty.statusEntries.destroyAll() instead.
        "::delete::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use EmpDuty.statusEntries.count() instead.
        "::count::empDuty::statusEntries": {
          url: urlBase + "/empDuties/:id/statusEntries/count",
          method: "GET",
        },

        // INTERNAL. Use Status.statusEntries.findById() instead.
        "::findById::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/:fk",
          method: "GET",
        },

        // INTERNAL. Use Status.statusEntries.destroyById() instead.
        "::destroyById::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Status.statusEntries.updateById() instead.
        "::updateById::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Status.statusEntries() instead.
        "::get::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Status.statusEntries.create() instead.
        "::create::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use Status.statusEntries.destroyAll() instead.
        "::delete::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use Status.statusEntries.count() instead.
        "::count::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries/count",
          method: "GET",
        },

        // INTERNAL. Use TowReqPic.statusEntry() instead.
        "::get::towReqPic::statusEntry": {
          url: urlBase + "/towReqPics/:id/statusEntry",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#empDuty
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Fetches belongsTo relation empDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `EmpDuty` object.)
         * </em>
         */
        R.empDuty = function() {
          var TargetResource = $injector.get("EmpDuty");
          var action = TargetResource["::get::statusEntry::empDuty"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#status
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Fetches belongsTo relation status
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        R.status = function() {
          var TargetResource = $injector.get("Status");
          var action = TargetResource["::get::statusEntry::status"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#towReq
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Fetches belongsTo relation towReq
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReq = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::statusEntry::towReq"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.StatusEntry.towReqPics
     * @object
     * @description
     *
     * The object `StatusEntry.towReqPics` groups methods
     * manipulating `TowReqPic` instances related to `StatusEntry`.
     *
     * Use {@link lbServices.StatusEntry#towReqPics} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#towReqPics
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Queries towReqPics of statusEntry.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::get::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry.towReqPics#count
         * @methodOf lbServices.StatusEntry.towReqPics
         *
         * @description
         *
         * Counts towReqPics of statusEntry.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.count = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::count::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry.towReqPics#create
         * @methodOf lbServices.StatusEntry.towReqPics
         *
         * @description
         *
         * Creates a new instance in towReqPics of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.create = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::create::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry.towReqPics#destroyAll
         * @methodOf lbServices.StatusEntry.towReqPics
         *
         * @description
         *
         * Deletes all towReqPics of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.towReqPics.destroyAll = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::delete::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry.towReqPics#destroyById
         * @methodOf lbServices.StatusEntry.towReqPics
         *
         * @description
         *
         * Delete a related item by id for towReqPics
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqPics
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.towReqPics.destroyById = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::destroyById::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry.towReqPics#findById
         * @methodOf lbServices.StatusEntry.towReqPics
         *
         * @description
         *
         * Find a related item by id for towReqPics
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqPics
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.findById = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::findById::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry.towReqPics#updateById
         * @methodOf lbServices.StatusEntry.towReqPics
         *
         * @description
         *
         * Update a related item by id for towReqPics
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqPics
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.updateById = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::updateById::statusEntry::towReqPics"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowReqPic
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowReqPic` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowReqPic",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towReqPics/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use TowReqPic.statusEntry() instead.
        "prototype$__get__statusEntry": {
          url: urlBase + "/towReqPics/:id/statusEntry",
          method: "GET",
        },

        // INTERNAL. Use TowReqPic.picType() instead.
        "prototype$__get__picType": {
          url: urlBase + "/towReqPics/:id/picType",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#create
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towReqPics",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#upsert
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towReqPics",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#exists
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/towReqPics/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#findById
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towReqPics/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#find
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towReqPics",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#findOne
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towReqPics/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#updateAll
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/towReqPics/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#deleteById
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towReqPics/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#count
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towReqPics/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#prototype$updateAttributes
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towReqPics/:id",
          method: "PUT",
        },

        // INTERNAL. Use StatusEntry.towReqPics.findById() instead.
        "::findById::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/:fk",
          method: "GET",
        },

        // INTERNAL. Use StatusEntry.towReqPics.destroyById() instead.
        "::destroyById::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use StatusEntry.towReqPics.updateById() instead.
        "::updateById::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/:fk",
          method: "PUT",
        },

        // INTERNAL. Use StatusEntry.towReqPics() instead.
        "::get::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use StatusEntry.towReqPics.create() instead.
        "::create::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics",
          method: "POST",
        },

        // INTERNAL. Use StatusEntry.towReqPics.destroyAll() instead.
        "::delete::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics",
          method: "DELETE",
        },

        // INTERNAL. Use StatusEntry.towReqPics.count() instead.
        "::count::statusEntry::towReqPics": {
          url: urlBase + "/statusEntries/:id/towReqPics/count",
          method: "GET",
        },

        // INTERNAL. Use PicType.towReqPics.findById() instead.
        "::findById::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/:fk",
          method: "GET",
        },

        // INTERNAL. Use PicType.towReqPics.destroyById() instead.
        "::destroyById::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use PicType.towReqPics.updateById() instead.
        "::updateById::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/:fk",
          method: "PUT",
        },

        // INTERNAL. Use PicType.towReqPics() instead.
        "::get::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use PicType.towReqPics.create() instead.
        "::create::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics",
          method: "POST",
        },

        // INTERNAL. Use PicType.towReqPics.destroyAll() instead.
        "::delete::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics",
          method: "DELETE",
        },

        // INTERNAL. Use PicType.towReqPics.count() instead.
        "::count::picType::towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/count",
          method: "GET",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#statusEntry
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Fetches belongsTo relation statusEntry
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntry = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::towReqPic::statusEntry"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReqPic#picType
         * @methodOf lbServices.TowReqPic
         *
         * @description
         *
         * Fetches belongsTo relation picType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        R.picType = function() {
          var TargetResource = $injector.get("PicType");
          var action = TargetResource["::get::towReqPic::picType"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.PicType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `PicType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "PicType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/picTypes/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use PicType.towReqPics.findById() instead.
        "prototype$__findById__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/:fk",
          method: "GET",
        },

        // INTERNAL. Use PicType.towReqPics.destroyById() instead.
        "prototype$__destroyById__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use PicType.towReqPics.updateById() instead.
        "prototype$__updateById__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/:fk",
          method: "PUT",
        },

        // INTERNAL. Use PicType.towReqPics() instead.
        "prototype$__get__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use PicType.towReqPics.create() instead.
        "prototype$__create__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics",
          method: "POST",
        },

        // INTERNAL. Use PicType.towReqPics.destroyAll() instead.
        "prototype$__delete__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics",
          method: "DELETE",
        },

        // INTERNAL. Use PicType.towReqPics.count() instead.
        "prototype$__count__towReqPics": {
          url: urlBase + "/picTypes/:id/towReqPics/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#create
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/picTypes",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#upsert
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/picTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#exists
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/picTypes/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#findById
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/picTypes/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#find
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/picTypes",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#findOne
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/picTypes/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#updateAll
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/picTypes/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#deleteById
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/picTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#count
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/picTypes/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PicType#prototype$updateAttributes
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PicType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/picTypes/:id",
          method: "PUT",
        },

        // INTERNAL. Use TowReqPic.picType() instead.
        "::get::towReqPic::picType": {
          url: urlBase + "/towReqPics/:id/picType",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.PicType.towReqPics
     * @object
     * @description
     *
     * The object `PicType.towReqPics` groups methods
     * manipulating `TowReqPic` instances related to `PicType`.
     *
     * Use {@link lbServices.PicType#towReqPics} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.PicType#towReqPics
         * @methodOf lbServices.PicType
         *
         * @description
         *
         * Queries towReqPics of picType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::get::picType::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PicType.towReqPics#count
         * @methodOf lbServices.PicType.towReqPics
         *
         * @description
         *
         * Counts towReqPics of picType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.count = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::count::picType::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PicType.towReqPics#create
         * @methodOf lbServices.PicType.towReqPics
         *
         * @description
         *
         * Creates a new instance in towReqPics of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.create = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::create::picType::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PicType.towReqPics#destroyAll
         * @methodOf lbServices.PicType.towReqPics
         *
         * @description
         *
         * Deletes all towReqPics of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.towReqPics.destroyAll = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::delete::picType::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PicType.towReqPics#destroyById
         * @methodOf lbServices.PicType.towReqPics
         *
         * @description
         *
         * Delete a related item by id for towReqPics
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqPics
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.towReqPics.destroyById = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::destroyById::picType::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PicType.towReqPics#findById
         * @methodOf lbServices.PicType.towReqPics
         *
         * @description
         *
         * Find a related item by id for towReqPics
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqPics
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.findById = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::findById::picType::towReqPics"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PicType.towReqPics#updateById
         * @methodOf lbServices.PicType.towReqPics
         *
         * @description
         *
         * Update a related item by id for towReqPics
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for towReqPics
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReqPic` object.)
         * </em>
         */
        R.towReqPics.updateById = function() {
          var TargetResource = $injector.get("TowReqPic");
          var action = TargetResource["::updateById::picType::towReqPics"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Party
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Party` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Party",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/parties/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Party.duties.findById() instead.
        "prototype$__findById__duties": {
          url: urlBase + "/parties/:id/duties/:fk",
          method: "GET",
        },

        // INTERNAL. Use Party.duties.destroyById() instead.
        "prototype$__destroyById__duties": {
          url: urlBase + "/parties/:id/duties/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Party.duties.updateById() instead.
        "prototype$__updateById__duties": {
          url: urlBase + "/parties/:id/duties/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Party.sites.findById() instead.
        "prototype$__findById__sites": {
          url: urlBase + "/parties/:id/sites/:fk",
          method: "GET",
        },

        // INTERNAL. Use Party.sites.destroyById() instead.
        "prototype$__destroyById__sites": {
          url: urlBase + "/parties/:id/sites/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use Party.sites.updateById() instead.
        "prototype$__updateById__sites": {
          url: urlBase + "/parties/:id/sites/:fk",
          method: "PUT",
        },

        // INTERNAL. Use Party.duties() instead.
        "prototype$__get__duties": {
          url: urlBase + "/parties/:id/duties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Party.duties.create() instead.
        "prototype$__create__duties": {
          url: urlBase + "/parties/:id/duties",
          method: "POST",
        },

        // INTERNAL. Use Party.duties.destroyAll() instead.
        "prototype$__delete__duties": {
          url: urlBase + "/parties/:id/duties",
          method: "DELETE",
        },

        // INTERNAL. Use Party.duties.count() instead.
        "prototype$__count__duties": {
          url: urlBase + "/parties/:id/duties/count",
          method: "GET",
        },

        // INTERNAL. Use Party.sites() instead.
        "prototype$__get__sites": {
          url: urlBase + "/parties/:id/sites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use Party.sites.create() instead.
        "prototype$__create__sites": {
          url: urlBase + "/parties/:id/sites",
          method: "POST",
        },

        // INTERNAL. Use Party.sites.destroyAll() instead.
        "prototype$__delete__sites": {
          url: urlBase + "/parties/:id/sites",
          method: "DELETE",
        },

        // INTERNAL. Use Party.sites.count() instead.
        "prototype$__count__sites": {
          url: urlBase + "/parties/:id/sites/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#create
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/parties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#upsert
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/parties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#exists
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/parties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#findById
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/parties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#find
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/parties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#findOne
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/parties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#updateAll
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/parties/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#deleteById
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/parties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#count
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/parties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Party#prototype$updateAttributes
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/parties/:id",
          method: "PUT",
        },

        // INTERNAL. Use Duty.party() instead.
        "::get::duty::party": {
          url: urlBase + "/reqDuties/:id/party",
          method: "GET",
        },

        // INTERNAL. Use StatusRight.party() instead.
        "::get::statusRight::party": {
          url: urlBase + "/statusRights/:id/party",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.Party.duties
     * @object
     * @description
     *
     * The object `Party.duties` groups methods
     * manipulating `Duty` instances related to `Party`.
     *
     * Use {@link lbServices.Party#duties} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Party#duties
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Queries duties of party.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        R.duties = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::get::party::duties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.duties#count
         * @methodOf lbServices.Party.duties
         *
         * @description
         *
         * Counts duties of party.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        R.duties.count = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::count::party::duties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.duties#create
         * @methodOf lbServices.Party.duties
         *
         * @description
         *
         * Creates a new instance in duties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        R.duties.create = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::create::party::duties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.duties#destroyAll
         * @methodOf lbServices.Party.duties
         *
         * @description
         *
         * Deletes all duties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.duties.destroyAll = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::delete::party::duties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.duties#destroyById
         * @methodOf lbServices.Party.duties
         *
         * @description
         *
         * Delete a related item by id for duties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for duties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.duties.destroyById = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::destroyById::party::duties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.duties#findById
         * @methodOf lbServices.Party.duties
         *
         * @description
         *
         * Find a related item by id for duties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for duties
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        R.duties.findById = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::findById::party::duties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.duties#updateById
         * @methodOf lbServices.Party.duties
         *
         * @description
         *
         * Update a related item by id for duties
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for duties
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Duty` object.)
         * </em>
         */
        R.duties.updateById = function() {
          var TargetResource = $injector.get("Duty");
          var action = TargetResource["::updateById::party::duties"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Party.sites
     * @object
     * @description
     *
     * The object `Party.sites` groups methods
     * manipulating `Site` instances related to `Party`.
     *
     * Use {@link lbServices.Party#sites} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Party#sites
         * @methodOf lbServices.Party
         *
         * @description
         *
         * Queries sites of party.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::get::party::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.sites#count
         * @methodOf lbServices.Party.sites
         *
         * @description
         *
         * Counts sites of party.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.count = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::count::party::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.sites#create
         * @methodOf lbServices.Party.sites
         *
         * @description
         *
         * Creates a new instance in sites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.create = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::create::party::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.sites#destroyAll
         * @methodOf lbServices.Party.sites
         *
         * @description
         *
         * Deletes all sites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.sites.destroyAll = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::delete::party::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.sites#destroyById
         * @methodOf lbServices.Party.sites
         *
         * @description
         *
         * Delete a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.sites.destroyById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::destroyById::party::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.sites#findById
         * @methodOf lbServices.Party.sites
         *
         * @description
         *
         * Find a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.findById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::findById::party::sites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Party.sites#updateById
         * @methodOf lbServices.Party.sites
         *
         * @description
         *
         * Update a related item by id for sites
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sites
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Site` object.)
         * </em>
         */
        R.sites.updateById = function() {
          var TargetResource = $injector.get("Site");
          var action = TargetResource["::updateById::party::sites"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.StatusOrder
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `StatusOrder` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "StatusOrder",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/statusOrders/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#create
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusOrder` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/statusOrders",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#upsert
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusOrder` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/statusOrders",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#exists
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/statusOrders/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#findById
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusOrder` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/statusOrders/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#find
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusOrder` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/statusOrders",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#findOne
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusOrder` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/statusOrders/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#updateAll
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/statusOrders/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#deleteById
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/statusOrders/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#count
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/statusOrders/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusOrder#prototype$updateAttributes
         * @methodOf lbServices.StatusOrder
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusOrder` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/statusOrders/:id",
          method: "PUT",
        },
      }
    );





    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.StatusRight
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `StatusRight` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "StatusRight",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/statusRights/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use StatusRight.status() instead.
        "prototype$__get__status": {
          url: urlBase + "/statusRights/:id/status",
          method: "GET",
        },

        // INTERNAL. Use StatusRight.party() instead.
        "prototype$__get__party": {
          url: urlBase + "/statusRights/:id/party",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#create
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusRight` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/statusRights",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#upsert
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusRight` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/statusRights",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#exists
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/statusRights/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#findById
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusRight` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/statusRights/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#find
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusRight` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/statusRights",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#findOne
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusRight` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/statusRights/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#updateAll
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/statusRights/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#deleteById
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/statusRights/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#count
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/statusRights/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#prototype$updateAttributes
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusRight` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/statusRights/:id",
          method: "PUT",
        },
      }
    );





        /**
         * @ngdoc method
         * @name lbServices.StatusRight#status
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Fetches belongsTo relation status
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        R.status = function() {
          var TargetResource = $injector.get("Status");
          var action = TargetResource["::get::statusRight::status"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusRight#party
         * @methodOf lbServices.StatusRight
         *
         * @description
         *
         * Fetches belongsTo relation party
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Party` object.)
         * </em>
         */
        R.party = function() {
          var TargetResource = $injector.get("Party");
          var action = TargetResource["::get::statusRight::party"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TransType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TransType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TransType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/transTypes/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use TransType.vehicles.findById() instead.
        "prototype$__findById__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/:fk",
          method: "GET",
        },

        // INTERNAL. Use TransType.vehicles.destroyById() instead.
        "prototype$__destroyById__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/:fk",
          method: "DELETE",
        },

        // INTERNAL. Use TransType.vehicles.updateById() instead.
        "prototype$__updateById__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/:fk",
          method: "PUT",
        },

        // INTERNAL. Use TransType.vehicles() instead.
        "prototype$__get__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use TransType.vehicles.create() instead.
        "prototype$__create__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use TransType.vehicles.destroyAll() instead.
        "prototype$__delete__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use TransType.vehicles.count() instead.
        "prototype$__count__vehicles": {
          url: urlBase + "/transTypes/:id/vehicles/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#create
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/transTypes",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#upsert
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/transTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#exists
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/transTypes/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#findById
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/transTypes/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#find
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/transTypes",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#findOne
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/transTypes/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#updateAll
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/transTypes/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#deleteById
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/transTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#count
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/transTypes/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TransType#prototype$updateAttributes
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TransType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/transTypes/:id",
          method: "PUT",
        },

        // INTERNAL. Use Vehicle.transType() instead.
        "::get::vehicle::transType": {
          url: urlBase + "/vehicles/:id/transType",
          method: "GET",
        },
      }
    );




    /**
     * @ngdoc object
     * @name lbServices.TransType.vehicles
     * @object
     * @description
     *
     * The object `TransType.vehicles` groups methods
     * manipulating `Vehicle` instances related to `TransType`.
     *
     * Use {@link lbServices.TransType#vehicles} to query
     * all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.TransType#vehicles
         * @methodOf lbServices.TransType
         *
         * @description
         *
         * Queries vehicles of transType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::get::transType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TransType.vehicles#count
         * @methodOf lbServices.TransType.vehicles
         *
         * @description
         *
         * Counts vehicles of transType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.count = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::count::transType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TransType.vehicles#create
         * @methodOf lbServices.TransType.vehicles
         *
         * @description
         *
         * Creates a new instance in vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.create = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::create::transType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TransType.vehicles#destroyAll
         * @methodOf lbServices.TransType.vehicles
         *
         * @description
         *
         * Deletes all vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.vehicles.destroyAll = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::delete::transType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TransType.vehicles#destroyById
         * @methodOf lbServices.TransType.vehicles
         *
         * @description
         *
         * Delete a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        R.vehicles.destroyById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::destroyById::transType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TransType.vehicles#findById
         * @methodOf lbServices.TransType.vehicles
         *
         * @description
         *
         * Find a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.findById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::findById::transType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TransType.vehicles#updateById
         * @methodOf lbServices.TransType.vehicles
         *
         * @description
         *
         * Update a related item by id for vehicles
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for vehicles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.updateById = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::updateById::transType::vehicles"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.User
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `User` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "User",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/Users/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.User#login
         * @methodOf lbServices.User
         *
         * @description
         *
         * Login a user with username/email and password
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * The response body contains properties of the AccessToken created on login.
         * Depending on the value of `include` parameter, the body may contain additional properties:
         * 
         *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
         * 
         *
         */
        "login": {
          url: urlBase + "/Users/login",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#logout
         * @methodOf lbServices.User
         *
         * @description
         *
         * Logout a user with access token
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "logout": {
          url: urlBase + "/Users/logout",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#confirm
         * @methodOf lbServices.User
         *
         * @description
         *
         * Confirm a user registration with email verification token
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `uid` – `{string}` - 
         *
         *  - `token` – `{string}` - 
         *
         *  - `redirect` – `{string}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "confirm": {
          url: urlBase + "/Users/confirm",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#resetPassword
         * @methodOf lbServices.User
         *
         * @description
         *
         * Reset password for a user with email
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "resetPassword": {
          url: urlBase + "/Users/reset",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__findById__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find a related item by id for accessTokens
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__findById__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/:fk",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__destroyById__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a related item by id for accessTokens
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "prototype$__destroyById__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/:fk",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__updateById__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update a related item by id for accessTokens
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__updateById__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/:fk",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__get__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Queries accessTokens of User.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__get__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__create__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__create__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__delete__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__delete__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__count__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Counts accessTokens of User.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__count__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#create
         * @methodOf lbServices.User
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/Users",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#upsert
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/Users",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#exists
         * @methodOf lbServices.User
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/Users/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#findById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/Users/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#find
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/Users",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.User#findOne
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/Users/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#updateAll
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/Users/update",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#deleteById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/Users/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#count
         * @methodOf lbServices.User
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/Users/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$updateAttributes
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/Users/:id",
          method: "PUT",
        },
      }
    );





    return R;
  }]);


module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId'];

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    }

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    }

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = '$LoopBack$' + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = '$LoopBack$' + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {
          if (LoopBackAuth.accessTokenId) {
            config.headers.authorization = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 } },
              status: 401,
              config: config,
              headers: function() { return undefined; }
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        }
      }
    }])
  .factory('LoopBackResource', [ '$resource', function($resource) {
    return function(url, params, actions) {
      var resource = $resource(url, params, actions);

      // Angular always calls POST on $save()
      // This hack is based on
      // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
      resource.prototype.$save = function(success, error) {
        // Fortunately, LoopBack provides a convenient `upsert` method
        // that exactly fits our needs.
        var result = resource.upsert.call(this, {}, this, success, error);
        return result.$promise || result;
      }

      return resource;
    };
  }]);

})(window, window.angular);
